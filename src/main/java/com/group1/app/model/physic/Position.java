package com.group1.app.model.physic;

public class Position {

    private Integer x;
    private Integer y;

    public Position() {
        x = null;
        y = null;
    }

    public Position(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public Integer getX() {
        return x;
    }

    void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    void setY(Integer y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("x: %d, y: %d", x, y);
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(x) + Integer.hashCode(y);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        Position other = (Position) obj;
        return other.getY().equals(y) && other.getX().equals(x);
    }
}
