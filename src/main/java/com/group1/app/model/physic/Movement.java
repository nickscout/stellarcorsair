package com.group1.app.model.physic;

public class Movement {

    private int maxVelocity;
    private int acceleration;

    private int angle;
    private int velocity;

    private int time;

    public Movement(int maxVelocity, int acceleration) {
        this.maxVelocity = maxVelocity;
        this.acceleration = acceleration;

        angle = 0;
        velocity = 0;
        time = 0;
    }

    public int getMaxVelocity() {
        return maxVelocity;
    }

    public int getAcceleration() {
        return acceleration;
    }

    public int getAngle() {
        return angle;
    }

    public int getTime() {
        return time;
    }

    public void setAngle(int angle) {
        if (angle < 0) {
            setAngle(360 + angle);
            return;
        }
        if (angle > 360) {
            setAngle(angle - 360);
            return;
        }

        this.angle = angle;
    }

    public int getVelocity() {
        return velocity;
    }

    public void setVelocity(int velocity) {
        this.velocity = velocity < maxVelocity ? velocity : maxVelocity;
        if (0 > velocity || velocity >= maxVelocity) {
            time = 0;
        }
    }

    void accelerate() {


        velocity += acceleration*time;
        time++;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(getMaxVelocity()) +
                Integer.hashCode(getAcceleration()) +
                Integer.hashCode(getAngle()) +
                Integer.hashCode(getVelocity());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        Movement other = (Movement) obj;
        return other.getMaxVelocity() == maxVelocity &&
                other.getAcceleration() == acceleration &&
                other.getAngle() == angle &&
                other.getVelocity() == velocity;
    }

    @Override
    public String toString() {
        return String.format("" +
                        "max_velocity: %d, " +
                        "acceleration: %d, " +
                        "angle: %d, " +
                        "velocity: %d",
                maxVelocity, acceleration, angle, velocity);
    }
}
