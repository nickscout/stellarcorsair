package com.group1.app.model.physic;

public class Geometry {

    public static int getAngle(Position from, Position to) {
        int angle = (int) (Math.round(
                Math.toDegrees(
                        Math.atan2(to.getY() - from.getY(), to.getX() - from.getX()))));
        return angle;
    }

    public static int calculateDistance(Position from, Position to) {
        if (from.getX() == null || from.getY() == null || to.getX() == null || to.getY() == null)
            return 0;
        int distance = (int) Math.round(
                Math.sqrt(
                        Math.pow(from.getX() - to.getX(), 2) + Math.pow(from.getY() - to.getY(), 2)));
        return distance;
    }

    public static boolean isInCircle(Position obj, Position center, int radius) {
        return calculateDistance(obj, center) < radius;
    }
}
