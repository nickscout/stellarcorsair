package com.group1.app.model.physic;

public abstract class Physical {

    private Position position;
    private Movement movement;

    protected Physical(int maxVelocity, int acceleration) {
        position = new Position();
        movement = new Movement(maxVelocity, acceleration);
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(int x, int y) {
        position.setX(x);
        position.setY(y);
    }

    public void setPosition(Position position) {
        this.position.setX(position.getX());
        this.position.setY(position.getY());
    }

    public Movement getMovement() {
        return movement;
    }

    public void step(int angle, int velocity) {
        movement.setAngle(angle);
        //if motion just started or max velocity reached
        if (movement.getTime() == 0 && movement.getVelocity() != movement.getMaxVelocity()) {
            movement.setVelocity(velocity);
        }
        int x = (int) (getPosition().getX() +
                Math.cos(Math.toRadians(movement.getAngle())) * movement.getVelocity());
        int y = (int) (getPosition().getY() +
                Math.sin(Math.toRadians(movement.getAngle())) * movement.getVelocity());
        setPosition(x, y);
        movement.accelerate();
    }

    public void step(Position destination) {
        if (getPosition().equals(destination)) {
            movement.setVelocity(0);
            return;
        }

        double xScalar = destination.getX() - getPosition().getX();
        double yScalar = destination.getY() - getPosition().getY();
        int distance = (int) Math.round(Math.sqrt(yScalar * yScalar + xScalar * xScalar));
        int angle = (int) Math.round(Math.toDegrees(Math.atan2(yScalar, xScalar)));

        movement.setAngle(angle);

        if (distance > movement.getVelocity()) {
            int x = (int) (getPosition().getX() + Math.cos(Math.toRadians(angle)) * movement.getVelocity());
            int y = (int) (getPosition().getY() + Math.sin(Math.toRadians(angle)) * movement.getVelocity());

            setPosition(x, y);
            movement.accelerate();
        } else {
            getPosition().setX(destination.getX());
            getPosition().setY(destination.getY());

            movement.setVelocity(0);
        }
    }
}
