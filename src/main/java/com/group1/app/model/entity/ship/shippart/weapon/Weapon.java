package com.group1.app.model.entity.ship.shippart.weapon;

import com.group1.app.model.entity.Entity;
import com.group1.app.model.entity.ship.AbstractShip;

public abstract class Weapon {

    private int size;
    private int damageValue;

    private int cooldown;
    private int currentCooldown;

    private AbstractShip owner;

    public Weapon(int size, int damageValue, int cooldown) {
        this.size = size;
        this.damageValue = damageValue;

        this.cooldown = cooldown;
        currentCooldown = 0;

        owner = null;
    }

    public int getSize() {
        return size;
    }

    public int getDamageValue() {
        return damageValue;
    }

    public int getCooldown() {
        return cooldown;
    }

    public int getCurrentCooldown() {
        return currentCooldown;
    }

    public void setCurrentCooldown(int currentCooldown) {
        this.currentCooldown = currentCooldown;
    }

    public AbstractShip getOwner() {
        return owner;
    }

    public void setOwner(AbstractShip owner) {
        this.owner = owner;
    }

    public boolean isOnCooldown() {
        return currentCooldown > 0;
    }

    public abstract void fire(Entity target);
}
