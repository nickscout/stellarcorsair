package com.group1.app.model.entity.ship.shippart.weapon;

import com.group1.app.controller.Narrator;
import com.group1.app.model.entity.Entity;
import com.group1.app.model.physic.Geometry;
import com.group1.app.model.physic.Movement;
import javafx.scene.image.Image;

public class Projectile extends Entity {

    private final int DAMAGE_RANGE;
    private final ProjectileGun SHOOTER_GUN;

    public Projectile(Narrator narrator, Movement movement, int DAMAGE_RANGE, ProjectileGun SHOOTER_GUN) {
        super(narrator, movement.getMaxVelocity(), movement.getAcceleration());
        this.DAMAGE_RANGE = DAMAGE_RANGE;
        this.SHOOTER_GUN = SHOOTER_GUN;
        this.setPosition(SHOOTER_GUN.getOwner().getPosition());
        addAiTask((sender -> {
            sender.step(movement.getAngle(), movement.getVelocity());
            Narrator.getInstance().getSubscribers().stream()
                    .filter(entity -> !entity.equals(SHOOTER_GUN.getOwner()) && !entity.equals(this))
                    .forEach(entity -> {
                        if (Geometry.isInCircle(entity.getPosition(), sender.getPosition(), DAMAGE_RANGE)) {
                            entity.onDamage(SHOOTER_GUN.getDamageValue(), SHOOTER_GUN.getOwner(), SHOOTER_GUN);
                            Narrator.getInstance().removeSubscriber(this);
                        }
                    });
        }));
    }

    @Override
    public void onDamage(int damageValue, Entity aggressor, Weapon weapon) {
        // ...
    }

    @Override
    public Image getImage() {
        return new Image("image/Projectile.png", 20, 20, true, true);
    }
}
