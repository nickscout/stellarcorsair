package com.group1.app.model.entity.ai;

import com.group1.app.model.entity.Entity;

public interface ProbablyAggressive {

    boolean isAggressiveTowards(Entity entity);

    void engage(Entity target);
}
