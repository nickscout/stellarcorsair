package com.group1.app.model.entity.ship;

public interface Destroyable {

    void onDestroy();
}
