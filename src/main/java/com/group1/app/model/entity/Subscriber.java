package com.group1.app.model.entity;

public interface Subscriber {

    void onTick();
}
