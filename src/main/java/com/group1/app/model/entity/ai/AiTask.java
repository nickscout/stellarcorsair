package com.group1.app.model.entity.ai;

import com.group1.app.model.entity.Entity;

public interface AiTask {

    void act(Entity sender);
}
