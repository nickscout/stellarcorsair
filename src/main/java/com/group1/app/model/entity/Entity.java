package com.group1.app.model.entity;

import com.group1.app.controller.Narrator;
import com.group1.app.model.entity.ai.AiTask;
import com.group1.app.model.entity.ship.Damageable;
import com.group1.app.model.entity.ship.Destroyable;
import com.group1.app.model.physic.Geometry;
import com.group1.app.model.physic.Physical;

import java.util.HashSet;
import java.util.Set;

public abstract class Entity extends Physical implements Subscriber, Destroyable, Damageable, Drawable {

    private Narrator narrator;

    private Set<AiTask> aiTasks;
    private Set<Entity> surroundings;

    private final int SURROUNDINGS_RADIUS = 370;

    public Entity(Narrator narrator, int maxVelocity, int acceleration) {
        super(maxVelocity, acceleration);
        this.narrator = narrator;
        narrator.addSubscriber(this);
        aiTasks = new HashSet<>();
        surroundings = new HashSet<>();
        addUpdateSurroundingsTask();
    }

    public Narrator getNarrator() {
        return narrator;
    }

    public void addAiTask(AiTask task) {
        aiTasks.add(task);
    }

    public void removeAiTask(AiTask task) {
        aiTasks.remove(task);
    }

    public Set<Entity> getSurroundings() {
        return surroundings;
    }

    public int getSurroundingsRadius() {
        return SURROUNDINGS_RADIUS;
    }

    private void addUpdateSurroundingsTask() {
        addAiTask(sender -> narrator.getSubscribers()
                .stream()
                .filter(subscriber -> !subscriber.equals(sender))
                .filter(subscriber -> Geometry.isInCircle(sender.getPosition(), getPosition(), SURROUNDINGS_RADIUS))
                .forEach(subscriber -> sender.getSurroundings().add((Entity) subscriber)));
    }

    public void onTick() {
        aiTasks.forEach((task -> task.act(this)));
    }

    @Override
    public void onDestroy() {
        getNarrator().removeSubscriber(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        Entity other = (Entity) obj;
        return getPosition().getX().equals(other.getPosition().getX()) &&
                getPosition().getY().equals(other.getPosition().getY()) &&
                getMovement().getAngle() == other.getMovement().getAngle() &&
                getMovement().getVelocity() == other.getMovement().getVelocity() &&
                getMovement().getMaxVelocity() == other.getMovement().getMaxVelocity() &&
                getMovement().getAcceleration() == other.getMovement().getAcceleration();
    }
}