package com.group1.app.model.entity.ship;

import com.group1.app.model.entity.Entity;
import com.group1.app.model.entity.ship.shippart.weapon.Weapon;

public interface Damageable {

    void onDamage(int damageValue, Entity aggressor, Weapon weapon);
}
