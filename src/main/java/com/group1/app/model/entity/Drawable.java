package com.group1.app.model.entity;

import javafx.scene.image.Image;

public interface Drawable {

    Image getImage();
}
