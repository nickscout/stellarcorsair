package com.group1.app.model.entity.ship;

import com.group1.app.controller.Narrator;
import com.group1.app.model.entity.Entity;
import com.group1.app.model.entity.ship.shippart.weapon.Weapon;
import com.group1.app.view.Sound;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;

public class PlayerShip extends AbstractShip {

    private boolean isDamaged;
    private Image image;

    public PlayerShip(Narrator narrator, int maxVelocity, int acceleration, int shipSize) {
        super(narrator, maxVelocity, acceleration, shipSize);
        image = new Image("image/PlayerShip.png", 75, 75, true, true);
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public void onDamage(int damageValue, Entity aggressor, Weapon weapon) {
        Sound.damageSoundPlay();
        isDamaged = true;
        receiveHullDamage(damageValue);
    }

    @Override
    public void onDestroy() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Sorry, Elon! YOU DIED!");
        alert.showAndWait();
        System.exit(0);
    }

    public void shoot() {
        getWeapons().forEach(weapon -> weapon.fire(null));
    }

    @Override
    public void onTick() {
        if (isDamaged) {
            image = new Image("image/PlayerShipOnDamage.png", 75, 75, true, true);
            isDamaged = false;
        } else {
            image = new Image("image/PlayerShip.png", 75, 75, true, true);
        }

        int angle = getMovement().getAngle();
        int velocity = getMovement().getAngle();
        step(angle, velocity);
    }
}
