package com.group1.app.model.entity.spasebody;

import com.group1.app.controller.Narrator;
import com.group1.app.model.entity.Entity;

public abstract class AbstractSpaceObj extends Entity {

    public AbstractSpaceObj(Narrator narrator, int maxVelocity, int acceleration) {
        super(narrator, maxVelocity, acceleration);
    }
}
