package com.group1.app.model.entity.spasebody;

import com.group1.app.controller.Narrator;
import com.group1.app.model.entity.Entity;
import com.group1.app.model.entity.ship.shippart.weapon.Weapon;
import com.group1.app.model.physic.Position;
import javafx.scene.image.Image;

public final class Planet extends AbstractSpaceObj {

    private int orbitAngle;
    private Image image;

    public Planet(Narrator narrator, Image image, int x, int y) {
        super(narrator, 0, 0);
        setPosition(x, y);
        this.image = image;
    }

    public void setOrbit(Position center, int radius, int angleSpeed) {
        orbitAngle = 0;
        addAiTask(sender -> {
            int x = (int) (center.getX() + radius * Math.cos(Math.toRadians(orbitAngle)));
            int y = (int) (center.getY() + radius * Math.sin(Math.toRadians(orbitAngle)));

            setPosition(x, y);

            orbitAngle += angleSpeed;
            if (orbitAngle > 360) orbitAngle -= 360;
        });
    }

    @Override
    public void onDamage(int damageValue, Entity aggressor, Weapon weapon) {
        // ...
    }

    @Override
    public Image getImage() {
        return image;
    }
}
