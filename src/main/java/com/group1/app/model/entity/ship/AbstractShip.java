package com.group1.app.model.entity.ship;

import com.group1.app.controller.Narrator;
import com.group1.app.model.entity.Entity;
import com.group1.app.model.entity.ship.shippart.weapon.Weapon;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public abstract class AbstractShip extends Entity {

    private Set<Weapon> weapons;

    private int shipSize;
    private int occupiedShipSize;
    private int hullHp;

    AbstractShip(Narrator narrator, int maxVelocity, int acceleration, int shipSize) {
        super(narrator, maxVelocity, acceleration);

        weapons = new HashSet<>();
        this.shipSize = shipSize;
        occupiedShipSize = 0;
        hullHp = 100;

        addAiTask(sender -> {
            AbstractShip ship = (AbstractShip) sender;
            ship.getWeaponsStream().filter(Weapon::isOnCooldown).forEach(weapon -> {
                weapon.setCurrentCooldown(weapon.getCooldown() - 1);
            });
        });
    }

    public Set<Weapon> getWeapons() {
        return new HashSet<>(weapons);
    }

    public void addWeapon(Weapon weapon) {
        if (weapon.getSize() > shipSize - occupiedShipSize) {
            System.out.println("unable to add weapon; no more free space");
            return;
        }
        weapons.add(weapon);
        weapon.setOwner(this);
        occupiedShipSize += weapon.getSize();
    }

    public void removeWeapon(Weapon weapon) {
        weapons.remove(weapon);
    }

    public int getHullHp() {
        return hullHp;
    }

    public void receiveHullDamage(int damage) {
        hullHp -= damage;
        if (hullHp <= 0) {
            onDestroy();
        }
    }

    public void repairHull(int repair) {
        hullHp += repair;
    }

    public boolean isAlive() { return hullHp > 0; }

    public Stream<Weapon> getWeaponsStream() {
        return weapons.stream();
    }
}
