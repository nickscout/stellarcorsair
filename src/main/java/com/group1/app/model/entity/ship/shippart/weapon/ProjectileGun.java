package com.group1.app.model.entity.ship.shippart.weapon;

import com.group1.app.controller.Narrator;
import com.group1.app.model.entity.Entity;
import com.group1.app.model.entity.ship.AbstractShip;
import com.group1.app.model.physic.Movement;
import com.group1.app.view.Sound;

public class ProjectileGun extends Weapon {

    private int projectileSpeed;

    public ProjectileGun(int size, int damageValue, int cooldown, int projectileSpeed) {
        super(size, damageValue, cooldown);
        this.projectileSpeed = projectileSpeed;
    }

    public int getProjectileSpeed() {
        return projectileSpeed;
    }

    @Override
    public void fire(Entity target) {
        if (!isOnCooldown()) {
            if (target == null || ((AbstractShip) target).isAlive()) {
                Narrator narrator = getOwner().getNarrator();
                //Sound shootSoundPlay = new Sound();
                Movement parentMovement = getOwner().getMovement();
                Movement movement = new Movement(parentMovement.getVelocity() + projectileSpeed, 0);
                movement.setAngle(parentMovement.getAngle());
                movement.setVelocity(movement.getVelocity() + projectileSpeed);
                Projectile projectile = new Projectile(narrator, movement, 50, this);
                Sound.shootSoundPlay();
                projectile.addAiTask((sender -> {
                    if (sender.getPosition().getX() > 2000   || sender.getPosition().getY() > 1200) {
                        sender.onDestroy();
                    }
                }));
                setCurrentCooldown(getCooldown());
            }
        } else {
            setCurrentCooldown(getCurrentCooldown() - 3);
        }
    }
}
