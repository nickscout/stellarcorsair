package com.group1.app.model.entity.ship;

import com.group1.app.controller.Narrator;
import com.group1.app.model.entity.Entity;
import com.group1.app.model.entity.ai.ProbablyAggressive;
import com.group1.app.model.entity.ship.shippart.weapon.Weapon;
import com.group1.app.model.physic.Geometry;
import com.group1.app.model.physic.Position;
import com.group1.app.view.Sound;
import javafx.scene.image.Image;

import java.util.List;

public class NpcShip extends AbstractShip implements ProbablyAggressive {

    private boolean isHostile;
    private Position destination;
    private boolean isDamaged;
    private final int COMBAT_RADIUS = 200;

    private Image image;

    public NpcShip(Narrator narrator, int maxVelocity, int acceleration, int shiSize) {
        super(narrator, maxVelocity, acceleration, shiSize);

        this.destination = getPosition();
        isDamaged = false;

//        this.addAiTask(sender -> {
//            if (destination != null) {
//                sender.step(destination);
//            }
//        });

        image = new Image("image/NPC1.png", 50, 50, true, true);
    }

    public void moveNyWaipoints(List<Position> waypoints) {
        for (int i = 0; i < waypoints.size() - 1; i++) {
            if (waypoints.get(i).equals(getPosition())) {
                destination = waypoints.get(i + 1);
                return;
            }
        }
        destination = waypoints.get(0);
    }

    public Position getDestination() {
        return destination;
    }

    public void setDestination(Position destination) {
        this.destination = destination;
    }

    @Override
    public void setPosition(int x, int y) {
        super.setPosition(x, y);
    }

    public void setDestination(int x, int y) {
        setDestination(new Position(x, y));
    }

    public void setHostile(boolean hostile) {
        isHostile = hostile;
    }

    public void setDamaged(boolean damaged) {
        isDamaged = damaged;
    }

    @Override
    public boolean isAggressiveTowards(Entity entity) {
        return isHostile;
    }

    @Override
    public void onDamage(int damageValue, Entity aggressor, Weapon weapon) {
        isDamaged = true;

        if (Geometry.calculateDistance(getPosition(), aggressor.getPosition()) < getSurroundingsRadius()) {
            engage(aggressor);
        }
        Sound.damageSoundPlay();
        receiveHullDamage(damageValue);
        if (aggressor.equals(getNarrator().getPlayerShip())) {
            isHostile = true;
        }
    }

    @Override
    public void engage(Entity target) {
        if (((AbstractShip) target).isAlive()) {
            getMovement().setAngle(Geometry.getAngle(getPosition(), target.getPosition()));
            this.addAiTask(sender -> {
                if (((AbstractShip) target).isAlive()) {
                    if (!Geometry.isInCircle(target.getPosition(), getPosition(), COMBAT_RADIUS)) {
                        sender.step(target.getPosition());
                    } else {
                        sender.getMovement().setAngle(Geometry.getAngle(sender.getPosition(), target.getPosition()));
                        sender.getMovement().setVelocity(0);
                    }
                    ((NpcShip) sender).getWeapons().forEach(weapon -> weapon.fire(target));
                }
            });

        }
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public void onTick() {
        if (isDamaged) {
            image = new Image("image/NPC1OnDamage.png", 50, 50, true, true);
            isDamaged = false;
        } else {
            image = new Image("image/NPC1.png", 50, 50, true, true);
            super.onTick();
        }
    }
}
