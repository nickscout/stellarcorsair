package com.group1.app.view;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.transform.Rotate;

class Drawer {

    private GraphicsContext graphicsContext;

    Drawer(GraphicsContext graphicsContext) {
        this.graphicsContext = graphicsContext;
    }

    void clearCanvas() {
        graphicsContext.clearRect(0, 0,
                graphicsContext.getCanvas().getWidth(), graphicsContext.getCanvas().getHeight());
    }

    void drawRotatedImage(Image image, int angle, int x, int y) {
        graphicsContext.save();
        rotate(graphicsContext, angle, x + image.getWidth() / 2, y + image.getHeight() / 2);
        graphicsContext.drawImage(image, x, y);
        graphicsContext.restore();
    }

    private void rotate(GraphicsContext gc, double angle, double px, double py) {
        Rotate r = new Rotate(angle, px, py);
        gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
    }
}
