package com.group1.app.view;

import com.group1.app.controller.Narrator;
import com.group1.app.model.entity.Entity;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.StackPane;

import java.util.List;
import java.util.Set;

public class MainUIController {

    @FXML
    private Slider estimatedSpeedSlider;
    @FXML
    private Slider angleSlider;
    @FXML
    private StackPane stackPane;
    @FXML
    private Canvas canvas;
    @FXML
    private TextArea textArea;
    @FXML
    private Label currentSpeedLabel;
    @FXML
    private Label estimatedSpeedLabel;

    private Narrator narrator;
    private Drawer drawer;

    @FXML
    public void initialize() {
        stackPaneInitialize();
        canvasInitialize();
        estimatedSpeedInitialize();

        drawerInitialize();
        narratorInitialize();
    }

    private void stackPaneInitialize() {
        stackPane.setBackground(new Background(
                new BackgroundImage(
                        new Image("image/Space.png"),
                        null,
                        null,
                        null,
                        null)));
    }

    private void canvasInitialize() {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.strokeRect(
                0,              //x of the upper left corner
                0,              //y of the upper left corner
                graphicsContext.getCanvas().getWidth(),    //width of the rectangle
                graphicsContext.getCanvas().getHeight());  //height of the rectangle
        graphicsContext.setLineWidth(3);
    }

    private void estimatedSpeedInitialize() {
        estimatedSpeedSlider.valueProperty().addListener(((observable, oldValue, newValue) -> {
            estimatedSpeedLabel.setText(Double.toString(newValue.doubleValue()));
        }));
    }

    private void narratorInitialize() {
        narrator = Narrator.getInstance();
        narrator.setMainUIController(this);
    }

    private void drawerInitialize() {
        drawer = new Drawer(canvas.getGraphicsContext2D());
    }

    public double getEstimatedSpeed() {
        return Double.parseDouble(estimatedSpeedLabel.getText());
    }

    public int getAngle() {
        return (int) angleSlider.getValue();
    }

    public void setMessage(String message) {
//         TODO: check and erase old messages
//        textArea.setText(message);
    }

    public synchronized void drawEntities(Set<Entity> entities) {
        drawer.clearCanvas();
        for (Entity entity : entities) {
            drawer.drawRotatedImage(
                    entity.getImage(),
                    entity.getMovement().getAngle(),
                    entity.getPosition().getX(),
                    entity.getPosition().getY());
        }
    }

    @FXML
    public void onFireClick(ActionEvent actionEvent) {
        narrator.firePlayerShip();
    }

    @FXML
    public void onStartGameClick(ActionEvent actionEvent) {
        narrator.notifySubscribers();
    }
}
