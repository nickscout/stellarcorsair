package com.group1.app.view;

import java.io.File;
import java.nio.file.Paths;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

public class Sound {
    public static void shootSoundPlay() {
        AudioClip makeShootVoice = new AudioClip(Paths.get("src\\main\\resources\\sounds\\GunShoot.mp3").toUri().toString());
        makeShootVoice.play(1.0);
    }
    public static void damageSoundPlay() {
        AudioClip makeShootVoice = new AudioClip(Paths.get("src\\main\\resources\\sounds\\SingleExplosion.mp3").toUri().toString());
        makeShootVoice.play(1.0);
    }
    public static void backgroundSoundPlay() {
        String backgroundMusicPath = "src\\main\\resources\\sounds\\GamePlaybackMusic.wav";
        Media backgroundMusic = new Media(new File(backgroundMusicPath).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(backgroundMusic);
        mediaPlayer.play();
    }
}
