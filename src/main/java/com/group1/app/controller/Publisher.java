package com.group1.app.controller;

import com.group1.app.model.entity.Entity;

public interface Publisher {

    void addSubscriber(Entity subscriber);

    void removeSubscriber(Entity subscriber);

    void notifySubscribers();
}
