package com.group1.app.controller;

import com.group1.app.model.entity.Entity;
import com.group1.app.model.entity.ship.NpcShip;
import com.group1.app.model.entity.ship.PlayerShip;
import com.group1.app.model.entity.ship.shippart.weapon.ProjectileGun;
import com.group1.app.model.entity.spasebody.Planet;
import com.group1.app.model.physic.Position;
import com.group1.app.view.MainUIController;
import javafx.scene.image.Image;

import java.awt.*;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public final class Narrator implements Publisher {

    private static Narrator narrator = new Narrator();

    private final int GAME_SPEED = 75;

    private Set<Entity> subscribers;
    private MainUIController uiController;

    private PlayerShip playerShip;

    private Narrator() {
        subscribers = new HashSet<>();

        buildPlayerShip();
        buildPlanet();
        buildNpcShip();
    }

    public static Narrator getInstance() {
        return narrator;
    }

    private void buildPlayerShip() {
        playerShip = new PlayerShip(this, 70, 10, 10);
        playerShip.addWeapon(new ProjectileGun(1, 20, 3, 30));
        playerShip.setPosition(20, 275);
    }

    private void buildPlanet() {
        Planet earth = new Planet(this,
                new Image(("image/Earth.png"), 125, 125, true, true),
                100, 200);
        //earth.setOrbit(earth.getPosition(), 1, 1);
        System.out.println(earth.getPosition());
        Planet mars = new Planet(this,
                new Image(("image/Mars.png"), 125, 125, true, true),
                1200, 700);
        //mars.setOrbit(mars.getPosition(), 1, 0);
        System.out.println(mars.getPosition());
    }

    private void buildNpcShip() {
        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            NpcShip npcShip = new NpcShip(this, 10, 5, 10);
            npcShip.setPosition(random.nextInt(1200), random.nextInt(900));
            npcShip.addWeapon(new ProjectileGun(1, 10, 4, 20));
        }
    }

    public void setMainUIController(MainUIController uiController) {
        this.uiController = uiController;
    }

    public Set<Entity> getSubscribers() {
        return new HashSet<>(subscribers);
    }

    @Override
    public void addSubscriber(Entity subscriber) {
        subscribers.add(subscriber);
    }

    @Override
    public void removeSubscriber(Entity subscriber) {
        subscribers.remove(subscriber);
    }

    @Override
    public void notifySubscribers() {
        Thread thread = new Thread(() -> {
            while (true) {
                Set<Entity> entities = new HashSet<>(subscribers);

                int estimatedSpeed = (int) uiController.getEstimatedSpeed();
                int angle = uiController.getAngle();

                playerShip.getMovement().setVelocity(estimatedSpeed);
                playerShip.getMovement().setAngle(angle);

                for (Entity subscriber : entities) {
                    subscriber.onTick();
                }

                uiController.drawEntities(entities);

                try {
                    Thread.sleep(GAME_SPEED);
                } catch (InterruptedException ex) {
                    System.err.println("thread is interrupted");
                }
            }
        });
        thread.start();
    }

    public PlayerShip getPlayerShip() {
        return playerShip;
    }

    public void firePlayerShip() {
        playerShip.shoot();
    }
}
